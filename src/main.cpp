#include "Arkanoid Game/Arkanoid Game.h"

int main()
{
    ArkanoidGame* arkanoidGame = new ArkanoidGame();
    arkanoidGame->Play();
    delete arkanoidGame;
    return 0;
}
