#include "Arkanoid Game.h"
#include <math.h>
#include <iostream>

#include "raylib.h"
#include "General/Screen Config.h"
#include "GameObjects/Player.h"
#include "GameObjects/Ball.h"
#include "GameObjects/Bricks.h"
#include "General/Collision.h"

ArkanoidGame::ArkanoidGame()
{
    InitAudioDevice();
    InitWindow(screenWidth, screenHeight, "Algebra Arkanoid");
    SetTargetFPS(60);
    player = new Player();
    ball = new Ball();
    for (int i = 0; i < BRICKS_ROWS; i++)
    {
        for (int j = 0; j < BRICKS_COLUMNS; j++)
        {
            bricks[i][j] = new Brick();
        }
    }
    ResetGame();
}
ArkanoidGame::~ArkanoidGame()
{
    delete player;
    delete ball;
    for (int i = 0; i < BRICKS_ROWS; i++)
    {
        for (int j = 0; j < BRICKS_COLUMNS; j++)
        {
            delete bricks[i][j];
        }
    }
    CloseWindow();
}
void ArkanoidGame::ResetGame()
{
    player->SetRectangle({ screenWidth / 2, screenHeight * playerYPosition, screenWidth / 10 , 20 });
    player->SetCurrentLife(playerMaxLife);

    ball->SetPosition({ screenWidth / 2, screenHeight * playerYPosition - player->GetRectangle().height - ballStartOffset });
    ball->SetSpeed({ 0, 0 });
    ball->SetRadius(ballRadius);
    ball->SetActive(false);

    for (int i = 0; i < BRICKS_ROWS; i++)
    {
        for (int j = 0; j < BRICKS_COLUMNS; j++)
        {
            bricks[i][j]->SetRectangle({ j * brickSize.x + brickSize.x / 2, i * brickSize.y + initialBricksPosition, brickSize.x, brickSize.y });
            if ((i + j) % 2 == 0)
            {
                bricks[i][j]->SetBrickColor(SKYBLUE);
            }
            else
            {
                bricks[i][j]->SetBrickColor(DARKBLUE);
            }
            bricks[i][j]->SetActive(true);
        }
    }
}
void ArkanoidGame::ResetBall()
{
    player->SetRectangle({ screenWidth / 2, screenHeight * playerYPosition, screenWidth / 10 , 20 });
    ball->SetPosition({ screenWidth / 2, screenHeight * playerYPosition - player->GetRectangle().height - ballStartOffset });
    ball->SetSpeed({ 0, 0 });
    ball->SetActive(false);
}
void ArkanoidGame::Update()
{
    framesCounter++;
    if (!gameOver)
    {
        if (IsKeyPressed('P') || IsKeyPressed('p')) pause = !pause;

        if (!pause)
        {
            if (IsKeyDown(KEY_LEFT)) player->Move(Direction::LEFT);
            if ((player->GetRectangle().x - player->GetRectangle().width / 2) <= 0) player->Move(Direction::RIGHT);
            if (IsKeyDown(KEY_RIGHT)) player->Move(Direction::RIGHT);
            if ((player->GetRectangle().x + player->GetRectangle().width / 2) >= screenWidth) player->Move(Direction::LEFT);

            if (ball->GetActive())
            {
                ball->Update();
            }
            else
            {
                ball->SetPosition({ player->GetRectangle().x, ball->GetPosition().y });
                if (IsKeyPressed(KEY_SPACE))
                {
                    ball->Launch();
                }
            }

            if (((ball->GetPosition().x + ball->GetRadius()) >= screenWidth) || ((ball->GetPosition().x - ball->GetRadius()) <= 0)) ball->SetSpeed({ ball->GetSpeed().x * -1, ball->GetSpeed().y });
            if ((ball->GetPosition().y - ball->GetRadius()) <= 0) ball->SetSpeed({ ball->GetSpeed().x, ball->GetSpeed().y * -1 });
            if ((ball->GetPosition().y + ball->GetRadius()) >= screenHeight)
            {
                player->SetCurrentLife(player->GetCurrentLife() - 1);
                ResetBall();
            }
            if (CircleRecCollision(ball->GetPosition(), ball->GetRadius(), player->GetRectangle()))
            {                                                                                               // IMPORTANTE: Los (* 180 / PI) son porque la mierda math.h trabaja con radianes
                float hipotenusa = 0;
                float golpePelotaBarra = 0;
                float anguloDireccionInicial = 0;
                float anguloDireccionFinal = 0;
                if (ball->GetSpeed().y > 0)   // Para que solo choque cuando la pelota va hacia abajo
                {
                    std::cout << "----------------------------------------" << std::endl;

                    // ----- REBOTE NORMAL -----
                    hipotenusa = sqrtf(powf(ball->GetSpeed().y, 2) + powf(ball->GetSpeed().x, 2));                              // Se vuelve a calcular porque es afectada po la gravedad
                    std::cout << "Hipotenusa:             " << hipotenusa << std::endl;

                    anguloDireccionInicial = fabsf(asinf(ball->GetSpeed().y / hipotenusa) * 180 / PI);                    // Se calcula el �ngulo con el que viene la pelota

                    std::cout << "Angulo Inicial:         " << anguloDireccionInicial << std::endl;

                    // Angulo final: Depende su signo de la direcci�n de la pelota
                    if (ball->GetSpeed().x < 0)
                        anguloDireccionFinal = -fabsf(anguloDireccionInicial);
                    else if (ball->GetSpeed().x > 0)
                        anguloDireccionFinal = fabsf(anguloDireccionInicial);

                    std::cout << "Angulo Result:          " << anguloDireccionFinal << std::endl;
                    ball->SetSpeed({ (cosf(anguloDireccionFinal * PI / 180) * hipotenusa), -fabsf(sinf(anguloDireccionFinal * PI / 180) * hipotenusa) });
                    {
                        if (anguloDireccionFinal < 0) ball->SetSpeed({ ball->GetSpeed().x * -1, ball->GetSpeed().y });
                    }
                    std::cout << "Y':                     " << ball->GetSpeed().y << std::endl;
                    std::cout << "X':                     " << ball->GetSpeed().x << std::endl;

                    // ----- SUMA DE MODIFICACI�N DE BARRA -----
                    golpePelotaBarra = (maxAngle * (ball->GetPosition().x - player->GetRectangle().x)) / (player->GetRectangle().width / 2);   // �ngulo que modifica S/ donde impacte

                    if (golpePelotaBarra > maxAngle) golpePelotaBarra = maxAngle;
                    else if (golpePelotaBarra < -maxAngle) golpePelotaBarra = -maxAngle;

                    std::cout << "Modificion de la Barra: " << golpePelotaBarra << std::endl;

                    if (ball->GetPosition().x < player->GetRectangle().x) // Golpe en posicion de la Pelota con respecto a la Barra
                    {
                        if (ball->GetSpeed().x > 0)
                        {
                            golpePelotaBarra = -golpePelotaBarra;
                        }

                        anguloDireccionFinal += golpePelotaBarra;
                        if (anguloDireccionFinal < 35 && ball->GetSpeed().x < 0)
                        {
                            anguloDireccionFinal = -35;
                        }
                    }
                    else
                    {
                        anguloDireccionFinal -= golpePelotaBarra;
                        if (anguloDireccionFinal < 35 && ball->GetSpeed().x > 0)
                            anguloDireccionFinal = 35;
                    }
                    std::cout << "Direccion Final:        " << anguloDireccionFinal << std::endl;

                    ball->SetSpeed({ (cosf(anguloDireccionFinal * PI / 180) * hipotenusa), -fabsf(sinf(anguloDireccionFinal * PI / 180) * hipotenusa) });

                    if (anguloDireccionFinal < 0) ball->SetSpeed({ ball->GetSpeed().x * -1, ball->GetSpeed().y });

                    std::cout << "Y':                     " << ball->GetSpeed().y << std::endl;
                    std::cout << "X':                     " << ball->GetSpeed().x << std::endl;

                    pause = pause;  // CAMBIAR PAUSA
                }
            }

            for (int i = 0; i < BRICKS_ROWS; i++)
            {
                for (int j = 0; j < BRICKS_COLUMNS; j++)
                {
                    if (bricks[i][j]->IsActive())
                    {
                        // Hit below
                        if (((ball->GetPosition().y - ball->GetRadius()) <= (bricks[i][j]->GetRectangle().y + brickSize.y / 2)) &&
                            ((ball->GetPosition().y - ball->GetRadius()) > (bricks[i][j]->GetRectangle().y + brickSize.y / 2 + ball->GetSpeed().y)) &&
                            ((fabsf(ball->GetPosition().x - bricks[i][j]->GetRectangle().x)) < (brickSize.x / 2 + ball->GetRadius() * 2 / 3)) && (ball->GetSpeed().y < 0))
                        {
                            bricks[i][j]->SetActive(false);
                            ball->SetSpeed({ ball->GetSpeed().x,ball->GetSpeed().y * -1 });
                        }
                        // Hit above
                        else if (((ball->GetPosition().y + ball->GetRadius()) >= (bricks[i][j]->GetRectangle().y - brickSize.y / 2)) &&
                            ((ball->GetPosition().y + ball->GetRadius()) < (bricks[i][j]->GetRectangle().y - brickSize.y / 2 + ball->GetSpeed().y)) &&
                            ((fabsf(ball->GetPosition().x - bricks[i][j]->GetRectangle().x)) < (brickSize.x / 2 + ball->GetRadius() * 2 / 3)) && (ball->GetSpeed().y > 0))
                        {
                            bricks[i][j]->SetActive(false);
                            ball->SetSpeed({ ball->GetSpeed().x,ball->GetSpeed().y * -1 });
                        }
                        // Hit left
                        else if (((ball->GetPosition().x + ball->GetRadius()) >= (bricks[i][j]->GetRectangle().x - brickSize.x / 2)) &&
                            ((ball->GetPosition().x + ball->GetRadius()) < (bricks[i][j]->GetRectangle().x - brickSize.x / 2 + ball->GetSpeed().x)) &&
                            ((fabsf(ball->GetPosition().y - bricks[i][j]->GetRectangle().y)) < (brickSize.y / 2 + ball->GetRadius() * 2 / 3)) && (ball->GetSpeed().x > 0))
                        {
                            bricks[i][j]->SetActive(false);
                            ball->SetSpeed({ ball->GetSpeed().x * -1, ball->GetSpeed().y });
                        }
                        // Hit right
                        else if (((ball->GetPosition().x - ball->GetRadius()) <= (bricks[i][j]->GetRectangle().x + brickSize.x / 2)) &&
                            ((ball->GetPosition().x - ball->GetRadius()) > (bricks[i][j]->GetRectangle().x + brickSize.x / 2 + ball->GetSpeed().x)) &&
                            ((fabsf(ball->GetPosition().y - bricks[i][j]->GetRectangle().y)) < (brickSize.y / 2 + ball->GetRadius() * 2 / 3)) && (ball->GetSpeed().x < 0))
                        {
                            bricks[i][j]->SetActive(false);
                            ball->SetSpeed({ ball->GetSpeed().x * -1, ball->GetSpeed().y });
                        }
                    }
                }
            }

            if (player->GetCurrentLife() <= 0) gameOver = true;
            else
            {
                gameOver = true;

                for (int i = 0; i < BRICKS_ROWS; i++)
                {
                    for (int j = 0; j < BRICKS_COLUMNS; j++)
                    {
                        if (bricks[i][j]->IsActive()) gameOver = false;
                    }
                }
            }

        }
    }
    else
    {
        if (IsKeyPressed(KEY_ENTER))
        {
            ResetGame();
            gameOver = false;
        }
    }
}
void ArkanoidGame::Draw()
{
    BeginDrawing();
    ClearBackground(RAYWHITE);
    if (!gameOver)
    {
        player->Draw();
        ball->Draw();
        for (int i = 0; i < BRICKS_ROWS; i++)
        {
            for (int j = 0; j < BRICKS_COLUMNS; j++)
            {
                if(bricks[i][j]->IsActive())
                {
                    bricks[i][j]->Draw();
                }
            }
        }

        if (pause && ((framesCounter / 30) % 2)) DrawText("GAME PAUSED", screenWidth / 2 - MeasureText("GAME PAUSED", 40) / 2, screenHeight / 2 - 40, 40, GRAY);
    }
    else
    {
        DrawText("GAME OVER", GetScreenWidth() / 2 - MeasureText("GAME OVER", 50) / 2, GetScreenHeight() / 2 - 100, 50, RED);
        DrawText("PRESS [ENTER] TO PLAY AGAIN", GetScreenWidth() / 2 - MeasureText("PRESS [ENTER] TO PLAY AGAIN", 20) / 2, GetScreenHeight() / 2 - 50, 20, GRAY);
    }
    EndDrawing();
}
void ArkanoidGame::Play()
{
    while(!WindowShouldClose())
    {
        Update();
        Draw();
    }
}