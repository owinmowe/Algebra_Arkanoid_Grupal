#include "Collision.h"
#include <math.h>

bool CircleRecCollision(Vector2 pos, float radius, Rectangle rec)
{
    float testX = pos.x;
    float testY = pos.y;

    if (pos.x < rec.x - rec.width / 2)  testX = rec.x - rec.width / 2;
    else if (pos.x > rec.x + rec.width / 2) testX = + rec.width / 2;
    if (pos.y < rec.y - rec.height / 2)  testY = rec.y - rec.height / 2;

    float distX = pos.x - testX;
    float distY = pos.y - testY;
    float distance = sqrtf((distX * distX) + (distY * distY));

    if (distance <= radius) 
    {
        return true;
    }
    return false;
}