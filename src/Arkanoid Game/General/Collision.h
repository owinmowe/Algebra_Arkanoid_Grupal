#ifndef COLLISION_H
#define COLLISION_H

#include "raylib.h"

bool CircleRecCollision(Vector2 pos, float radius, Rectangle rec);

#endif // !COLLISION_H

