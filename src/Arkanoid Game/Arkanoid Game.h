#ifndef ARKANOID_GAME
#define ARKANOID_GAME

#include "GameObjects/Player.h"
#include "GameObjects/Ball.h"
#include "GameObjects/Bricks.h"

using namespace GameObjects;

class ArkanoidGame
{
public:
	ArkanoidGame();
	~ArkanoidGame();
	void Play();
private:
	Player* player;
	Ball* ball;
	Brick* bricks[BRICKS_ROWS][BRICKS_COLUMNS];
	bool gameOver = false;
	bool pause = false;
	int framesCounter = 0;
	void ResetGame();
	void ResetBall();
	void Update();
	void Draw();
};

#endif // !ARKANOID_GAME

