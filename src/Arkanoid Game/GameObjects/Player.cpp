#include "Player.h"
#include "Arkanoid Game/General/Screen Config.h"

namespace GameObjects
{

	Player::Player()
	{
		rectangle = { 0,0,0,0 };
		currentLife = 1;
	}
	void Player::SetRectangle(Rectangle rec)
	{
		rectangle = rec;
	}
	Rectangle Player::GetRectangle()
	{
		return rectangle;
	}
	void Player::SetCurrentLife(int life)
	{
		currentLife = life;
	}
	int Player::GetCurrentLife()
	{
		return currentLife;
	}
	void Player::Move(Direction dir)
	{
		if(dir == Direction::LEFT)
		{
			rectangle.x -= playerSpeed;
		}
		else
		{
			rectangle.x += playerSpeed;
		}
	}
	void Player::Draw()
	{
		DrawRectangle(static_cast<int>(rectangle.x - rectangle.width / 2), static_cast<int>(rectangle.y - rectangle.height / 2), static_cast<int>(rectangle.width), static_cast<int>(rectangle.height), DARKBLUE);
		for (int i = 0; i < currentLife; i++) DrawRectangle(20 + 40 * i, screenHeight - 30, 35, 10, SKYBLUE);
	}
}