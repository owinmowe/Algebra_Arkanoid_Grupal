#ifndef BRICKS_H
#define BRICKS_H

#include "raylib.h"
#include "Arkanoid Game/General/Screen Config.h"

namespace GameObjects
{
	const int BRICKS_ROWS = 5;
	const int BRICKS_COLUMNS = 20;
	const int initialBricksPosition = 50;
	const Vector2 brickSize = { screenWidth / BRICKS_COLUMNS, (float)40 };

	class Brick
	{
	public:
		Brick();
		Rectangle GetRectangle();
		bool IsActive();
		void SetRectangle(Rectangle rec);
		void SetActive(bool act);
		void SetBrickColor(Color col);
		void Draw();
	private:
		Rectangle rectangle;
		bool active;
		Color color;
	};
}

#endif // !BRICKS_H

