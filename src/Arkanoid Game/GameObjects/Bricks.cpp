#include "Bricks.h"

namespace GameObjects
{
	Brick::Brick()
	{
		rectangle = { 0,0,0,0 };
		color = BLACK;
		active = false;
	}
	Rectangle Brick::GetRectangle()
	{
		return rectangle;
	}
	bool Brick::IsActive()
	{
		return active;
	}
	void Brick::SetRectangle(Rectangle rec)
	{
		rectangle = rec;
	}
	void Brick::SetActive(bool act)
	{
		active = act;
	}
	void Brick::SetBrickColor(Color col)
	{
		color = col;
	}
	void Brick::Draw()
	{
		DrawRectangle(static_cast<int>(rectangle.x - rectangle.width / 2), static_cast<int>(rectangle.y - rectangle.height / 2), static_cast<int>(rectangle.width), static_cast<int>(rectangle.height), color);
	}
}