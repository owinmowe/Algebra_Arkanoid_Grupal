#include "Ball.h"

namespace GameObjects
{
	Ball::Ball()
	{
		position = { 0,0 };
		speed = { 0,0 };
		radius = 0;
		active = false;
	}
	void Ball::SetPosition(Vector2 pos)
	{
		position = pos;
	}
	void Ball::SetSpeed(Vector2 spd)
	{
		speed = spd;
	}
	void Ball::SetRadius(float rad)
	{
		radius = rad;
	}
	void Ball::SetActive(bool act)
	{
		active = act;
	}
	Vector2 Ball::GetPosition()
	{
		return position;
	}
	Vector2 Ball::GetSpeed()
	{
		return speed;
	}
	float Ball::GetRadius()
	{
		return radius;
	}
	bool Ball::GetActive()
	{
		return active;
	}
	void Ball::Update() 
	{
		speed.y += 0.0135f;       // Gravedad
		position.x += speed.x;
		position.y += speed.y;
	}
	void Ball::Launch()
	{
		speed = ballStartingSpeed;
		active = true;
	}
	void Ball::Draw()
	{
		DrawCircleV(position, radius, BLUE);
		DrawLine(static_cast<int>(position.x), static_cast<int>(position.y), static_cast<int>(position.x + speed.x * 100), static_cast<int>(position.y + speed.y * 100), GREEN);
	}
}