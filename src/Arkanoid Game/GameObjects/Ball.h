#ifndef BALL_H
#define BALL_H

#include "raylib.h"

namespace GameObjects
{

	const float ballRadius = 7;
	const int ballStartOffset = 10;
	const Vector2 ballStartingSpeed = {3.5f, -5.f};
	const float ballGravity = 0.0135f;
	const float maxAngle = 55;

	class Ball
	{
	public:
		Ball();
		void SetPosition(Vector2 pos);
		void SetSpeed(Vector2 spd);
		void SetRadius(float rad);
		void SetActive(bool act);
		Vector2 GetPosition();
		Vector2 GetSpeed();
		float GetRadius();
		bool GetActive();
		void Launch();
		void Update();
		void Draw();
	private:
		Vector2 position;
		Vector2 speed;
		float radius;
		bool active;
	};
}

#endif // !BALL_H
