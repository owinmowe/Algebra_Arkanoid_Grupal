#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

namespace GameObjects
{


	const int playerMaxLife = 5;
	const float playerYPosition = 7 / 8.f;
	const int playerSpeed = 5;
	const enum class Direction{LEFT, RIGHT};

	class Player
	{
	public:
		Player();
		void SetRectangle(Rectangle rec);
		void Move(Direction dir);
		Rectangle GetRectangle();
		void SetCurrentLife(int life);
		int GetCurrentLife();
		void Draw();
	private:
		Rectangle rectangle;
		int currentLife;
	};
}

#endif // !PLAYER_H

